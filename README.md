Stand `02/2022`

---
# Player Media Monitor (PM*2)

## Description
Fetch a stream of news articles based on a custom search value.

## Requirements

* [node version manager (nvm)](https://github.com/nvm-sh/nvm)
* [node package manager (npm)](https://www.npmjs.com/)

## Installation

```bash
> nvm use
> npm install
```

> **Setup environment !**<br />
> Do not forget to set up your environment variables (`.env`)

## Usage

### local

```bash
# simple run
> npm run start

# run with hot-reload
> npm run dev
```

serve the application at http://localhost:3001/api/status

---

## License

Use it!
