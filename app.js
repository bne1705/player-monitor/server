'use strict';

require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const path = require("path");

const app = express();
const port = 3001;
const environment = process.env.NODE_ENV || 'production';


/**
 * server settings and configuration
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(path.join(__dirname, '/public')));


/**
 * run server
 */
app.listen(port, () => {
    console.info(`## Server startet!`);
    console.info(`App listening on port ${port}`);
    console.info(`Environment configured: ${environment}`);
});

/**
 * define routes
 */
app.get('/api/status', async (req, res) => {
    const Database = require('./src/models/Mongo');
    const db = new Database('');
    let ping = await db.ping();

    res.json({
        success: true,
        api: `running`,
        database: ping
    });
});

const Me = require('./src/routes/me.route');
const meRoute = new Me(app);
meRoute.register();

const Tracking = require('./src/routes/tracking.route');
const trackingRoute = new Tracking(app);
trackingRoute.register();

const Stream = require('./src/routes/stream.route');
const streamRoute = new Stream(app);
streamRoute.register();