'use strict';

const userModel = require('mongodb');
const MongoClient = userModel.MongoClient;
const Logger = require('./../utils/Logger');

class Mongo {

    database = 'bean';
    url = process.env.DB_HOST;
    collection;
    client = new MongoClient(this.url);

    logger = new Logger('error', 'mongodb').register();

    constructor(collection) {
        this.collection = collection;
    }


    async ping() {
        try {
            await this.client.connect();
            return await this.client.db(this.database).command({ping: 1})
        } catch (err) {
            this.logger.error(`(ping) Something went wrong: ${err}`);
        } finally {
            await this.client.close();
        }
    }


    async findOne(query) {
        try {
            await this.client.connect();
            const database = this.client.db(this.database);
            const collection = database.collection(this.collection);

            const options = {};

            return await collection.findOne(query, options);
        } catch (err) {
            this.logger.error(`(findOne) Something went wrong: ${err}`);
        } finally {
            await this.client.close();
        }
    }

    async insert(doc) {
        try {
            await this.client.connect();
            const database = this.client.db(this.database);
            const collection = database.collection(this.collection);

            const result = await collection.insertOne(doc);
            return `A document was inserted with the _id: ${result.insertedId}`;
        } catch (err) {
            this.logger.error(`(insert) Something went wrong: ${err}`);
        } finally {
            await this.client.close();
        }
    }


    /**
     * update one item
     * @param query
     * @param doc
     */
    async update(query, doc) {
        try {
            await this.client.connect();
            const database = this.client.db(this.database);
            const collection = database.collection(this.collection);

            const options = {upsert: true};
            const updateDoc = {
                $set: doc,
            };
            const result = await collection.updateOne(query, updateDoc, options);
            return `${result.matchedCount} document(s) matched the filter, updated ${result.modifiedCount} document(s)`;
        } catch (err) {
            this.logger.error(`(update) Something went wrong: ${err}`);
        } finally {
            await this.client.close();
        }
    }

}

module.exports = Mongo;