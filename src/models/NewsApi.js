'use strict';

const axios = require("axios");
const qs = require("qs");


/**
 * News API
 * https://newsapi.org/docs
 */
class NewsApi {

    url = "https://newsapi.org/v2";
    apikey = process.env.NEWSAPI_KEY;

    constructor() {
        // ...
    }

    /**
     * > https://newsapi.org/docs/endpoints/everything
     * @param data
     * @returns {Promise<{succes: boolean, error}|{result: any, success: boolean}>}
     */
    async search(data) {
        let query = {
            q: data['searchValue'],
            searchIn: 'title,content',
            sortBy: 'publishedAt',
            pageSize: 6,
            page: 1
        }

        try {
            let resp = await axios.get(
                `${this.url}/everything?${qs.stringify(query)}`,
                {
                    headers: {"X-Api-Key": this.apikey}
                }
            );

            return {
                success: true,
                result: resp.data
            }
        } catch (error) {
            return {
                succes: false,
                error
            }
        }
    };


    /**
     * > https://newsapi.org/docs/endpoints/top-headlines
     * @param data
     * @returns {Promise<{succes: boolean, error}|{result: any, success: boolean}>}
     */
    async top(data) {
        let query = {
            q: `"${data['searchValue']}"`,
            category: 'general,sports',
            pageSize: 4,
            page: 1
        }

        try {
            let resp = await axios.get(
                `${this.url}/top-headlines?${qs.stringify(query)}`,
                {
                    headers: {"X-Api-Key": this.apikey}
                }
            );

            return {
                success: true,
                result: resp.data
            }
        } catch (error) {
            return {
                succes: false,
                error
            }
        }
    };

}

module.exports = NewsApi;