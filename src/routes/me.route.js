'use strict';

const ConfigController = require('./../controller/ConfigController')
const {resetWatchers} = require("nodemon/lib/monitor/watch");

class TrackingRoute {

    app;

    constructor(app) {
        this.app = app;
    }

    register() {

        // GET my subscription
        this.app.get(`/api/me/subscription`,
            async (req, res) => {
                const Config = new ConfigController();
                let result = await Config.find(req);

                return res.json({
                    success: true,
                    result
                });
            }
        );

        // CREATE or UPDATE my subscription
        this.app.post(`/api/me/subscription`,
            async (req, res) => {
                const Config = new ConfigController();
                let result = await Config.update(req);

                return res.json({
                    success: true,
                    result
                });
            }
        );

        // REMOVE a subscription of my watchlist
        this.app.delete(`/api/me/subscription`,
            async (req, res) => {
                const Config = new ConfigController();
                let result = await Config.delete(req);

                return res.json({
                    success: true,
                    result
                });
            }
        );

    }

}

module.exports = TrackingRoute;