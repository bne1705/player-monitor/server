'use strict';

const TrackingController = require('./../controller/TrackingController')

class TrackingRoute {

    app;

    constructor(app) {
        this.app = app;
    }

    register() {

        /**
         *
         */
        this.app.post(`/api/tracking`,
            async (req, res) => {
                const Tracking = new TrackingController();
                let result = await Tracking.set(req);

                return res.json({
                    success: true,
                    result
                });
            }
        );

    }

}

module.exports = TrackingRoute;