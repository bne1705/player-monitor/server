'use strict';

const NewsModule = require('./../models/NewsApi');

class StreamRoute {

    app;
    news;

    constructor(app) {
        this.app = app;
        this.news = new NewsModule();
    }

    register() {

        /**
         * "This endpoint suits article discovery and analysis"
         */
        this.app.post(`/api/stream`,
            async (req, res) => {
                const result = await this.news.search(req.body);

                return res.json(result);
            }
        );

        /**
         * "This endpoint suits article discovery and analysis"
         */
        this.app.post(`/api/stream/top`,
            async (req, res) => {
                const result = await this.news.top(req.body);

                return res.json(result);
            }
        );

    }

}

module.exports = StreamRoute;