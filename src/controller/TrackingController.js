'use strict';

const Database = require('./../models/Mongo');
const JWT = require("../utils/JWT");

class TrackingController {

    database = new Database('tracking');

    async set(request) {
        let doc = request.body;
        doc['timestamp'] = new Date();
        doc['user'] = JWT.id(request.headers);

        try {
            return await this.database.insert(doc);
        } catch (e) {
            console.error('Error ConfigController', e)
        }
    }

}

module.exports = TrackingController;