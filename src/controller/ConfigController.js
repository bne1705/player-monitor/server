'use strict';

const Database = require('./../models/Mongo');
const JWT = require("../utils/JWT");
const _findIndex = require('lodash.findindex');
const _set = require('lodash.set');
const _remove = require('lodash.remove');
const {v4: uuidv4} = require('uuid');

class ConfigController {

    database = new Database('config')


    /**
     * find "user-config-doc"
     * @param request
     * @returns {Promise<(Document & {_id: InferIdType<Document>})|undefined>}
     */
    async find(request) {
        const user = JWT.id(request.headers);

        try {
            return this.database.findOne({user});
        } catch (e) {
            console.error('Error ConfigController "find"', e)
        }
    }

    /**
     * create | update "user-config-doc"
     * @param request
     * @returns {Promise<{doc: ((Document & {_id: InferIdType<Document>})|{user: *, config: *}), message: string}>}
     */
    async update(request) {
        const user = JWT.id(request.headers);

        let config = request.body;
        config['updatedAt'] = new Date();

        try {
            let doc = await this.database.findOne({user});

            if (!doc) {
                // no user doc found -> create one
                config['id'] = uuidv4();
                doc = {user, config: [config]};
            } else {
                // user doc exist
                if (!config.id) {
                    // no config id / no config exist -> add config
                    config['id'] = uuidv4();
                    doc.config.push(config);
                } else {
                    // update config
                    const index = _findIndex(doc.config, (o) => {
                        return o.id === config.id
                    });

                    _set(doc.config, [index], config);
                }
            }

            let message = await this.database.update({user}, doc);

            return {doc, message};
        } catch (e) {
            console.error('Error ConfigController "update"', e)
        }
    }


    /**
     * remove a config from "user-config-doc"
     * @param request
     * @returns {Promise<{doc: ((Document & {_id: InferIdType<Document>})|{user: *, config: *}), message: string}>}
     */
    async delete(request) {
        const user = JWT.id(request.headers);

        let config = request.body;

        try {
            let doc = await this.database.findOne({user});

            if (doc) {
                _remove(doc.config, (o) => {
                    return o.id === config.id
                });
            }

            let message = await this.database.update({user}, doc);

            return {doc, message};
        } catch (e) {
            console.error('Error ConfigController "update"', e)
        }
    }

}

module.exports = ConfigController;