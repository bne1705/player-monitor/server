'use strict';

const {createLogger, transports, format} = require("winston");
const { combine, timestamp, label, printf } = format;

class Logger {

    logger;

    constructor(level, source) {
        const myFormat = printf(({ message }) => {
            return `[${new Date().toLocaleString('de-DE')}] ${level} "${source}": ${message}`;
        });

        this.logger = createLogger({
            level,
            format: myFormat,
            transports: [
                new transports.File({filename: './logs/error.log', level: 'error'}),
                new transports.File({filename: './logs/query.log', level: 'query'})
            ],
            // silent: false
        });

        //
        // If we're not in production then log to the `console` with the format:
        // `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
        //
        if (process.env.NODE_ENV !== 'production') {
            this.logger.add(new transports.Console({
                format: format.simple(),
            }));
        }
    }

    /**
     *
     * @returns {*}
     */
    register() {
        return this.logger;
    }
}

module.exports = Logger;